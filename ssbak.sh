#!/bin/bash -e
#
# 2019-02-09: dhoang77
#
BK_TOP_DIR=backup
SOURCE_DIR=/mnt/${BK_TOP_DIR}
DESTINATION_DIR=${BK_TOP_DIR}/rootfs
RSYNC_CMD='rsync -av --progress -h --one-file-system --bwlimit=90MiB'

WHERE_I_AM=$( df . | tail -n 1 | cut -d' ' -f 1 )

echo "Generate the file blkid.txt"
blkid | grep -v -e "^${WHERE_I_AM}" > blkid.txt
echo

echo "Generate the file lsblk.txt"
lsblk | grep -v -e "^${WHERE_I_AM}" > lsblk.txt
echo

echo "Generate the file df.txt"
df -Thl | grep -v -e "tmpfs" -e "^${WHERE_I_AM}" > df.txt
echo

if [ ! -d "${SOURCE_DIR}" ]; then
  echo "Create ${SOURCE_DIR} directory"
  mkdir -pv ${SOURCE_DIR}
  echo
fi

if [ ! -d "${DESTINATION_DIR}" ]; then
  echo "Create ${DESTINATION_DIR} directory"
  mkdir -pv ${DESTINATION_DIR}
  echo
fi

echo "Backup of the root filesystem"
mount --bind / ${SOURCE_DIR} -o ro && \
${RSYNC_CMD} --delete --exclude-from=exclude/${SOURCE_DIR##*/}.lst ${SOURCE_DIR} ${DESTINATION_DIR}
umount ${SOURCE_DIR}
echo

GREP_CMD=$( echo -n "grep "; cat df.txt | sed 1d | cut -d' ' -f 1 | sed -e 's@^@-e "@; s@$@"@' | tr \\012 " " )
MOUNT_POINTS=$( mount | eval "${GREP_CMD}" | cut -d' ' -f 3 )
for MP in ${MOUNT_POINTS}
do
  echo "Backup mount point ${MP}"
  SUBDIR_NAME=${BK_TOP_DIR}/$( echo ${MP} | sed -e 's@/@__@g' )
  echo "MP=${MP} / SUBDIR=${SUBDIR_NAME}"
  if [ ! -d "${SUBDIR_NAME}" ]; then
    echo "Create ${SUBDIR_NAME} directory"
    mkdir -pv ${SUBDIR_NAME}
  fi
  ${RSYNC_CMD} ${MP}/ ${SUBDIR_NAME}
  echo
done

echo "Generate file du.txt"
du -shx ${BK_TOP_DIR}/* 2>&1 > du.txt
echo
